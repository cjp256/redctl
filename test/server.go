// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package test

import (
	"context"
	"os"
	"testing"

	"io/ioutil"
	"net/http"
	"path/filepath"

	"gitlab.com/redfield/redctl/internal/client"
	"gitlab.com/redfield/redctl/internal/server"
)

var (
	testDirPath string
	testServer  *http.Server
	testClient  *client.Client
)

// Cleanup artifacts post test
func Cleanup(t *testing.T) {
	t.Log("cleaning up test client...")
	testClient.Close()

	t.Log("cleaning up test server...")
	if testServer != nil {
		t.Log("shutting down server...")
		testServer.Shutdown(context.Background())
		testServer = nil
	}

	if testDirPath != "" {
		os.RemoveAll(testDirPath)
	}

	testDirPath = ""
}

func startServer(t *testing.T, opts server.RedctlServerOptions) error {
	if err := server.Serve(opts); err != http.ErrServerClosed {
		t.Log("failed to serve https:", err)
		return err
	}
	t.Log("server shutdown")
	return nil
}

// SetupServer starts test service
func SetupServer(t *testing.T) server.RedctlServerOptions {
	setupCerts(t)

	opts := server.RedctlServerOptions{
		ServerUrl:  "unix://" + filepath.Join(testDirPath, "redctl.socket"),
		ServerPath: testDirPath,
		ImagesPath: filepath.Join(testDirPath, "images"),
	}

	go startServer(t, opts)

	return opts
}

// SetupClient establishs connection to test server
func SetupClient(t *testing.T) *client.Client {
	setupCerts(t)

	c := client.NewClient().
		WithServerUrl(defaultTestUrl).
		WithTimeout("30s")

	testClient = c

	return c
}
