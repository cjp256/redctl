// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package domain

import (
	"fmt"
	"reflect"

	"gitlab.com/redfield/redctl/api"
)

// GetProperty returns properties that match 'key' in a DomainConfiguration.
func (d *Domain) GetProperty(key string) ([]string, error) {
	dc := d.Config

	var result []string

	// Get the field name from key
	name, ok := fieldNameFromXLKey[key]
	if !ok {
		return result, fmt.Errorf("%v is not a valid xl key", key)
	}

	f := reflect.ValueOf(dc).Elem().FieldByName(name)

	switch f.Kind() {
	case reflect.String:
		result = append(result, f.String())

	case reflect.Slice, reflect.Array:
		for i := 0; i < f.Len(); i++ {
			// Get the value that f.Index(i) points to
			v := reflect.Indirect(f.Index(i))

			// Call MarshalText
			s := v.MethodByName("MarshalText").Call([]reflect.Value{})[0]

			// Need to assert that underlying type of s is []byte,
			// the we can cast as string
			result = append(result, string(s.Interface().([]byte)))
		}

	case reflect.Int32:
		v, ok := domainEventString[f.Interface().(api.DomainEvent)]
		if !ok {
			return result, fmt.Errorf("unable to map %v to domain event string", f.Interface())
		}

		result = append(result, v)

	default:
		return result, fmt.Errorf("encountered unexpected type '%v'", f.Type())
	}

	return result, nil
}

// SetProperty sets a property in 'dc', using the key-value pair provided.
// If the property already exists, it is update with 'value'.
// Else, the new property is added, provided it is valid.
func (d *Domain) SetProperty(key string, value string) error {
	dc := d.Config

	// Get the field name to set
	name, ok := fieldNameFromXLKey[key]
	if !ok {
		return fmt.Errorf("%v is not a valid key", key)
	}

	// Special case: device_model_args are a list of arbitrary options, which are passed
	// as options to the device-model. Simply append the key to the slice.
	if name == fieldNameFromXLKey["device_model_args"] {
		dc.DeviceModelArgs = append(dc.DeviceModelArgs, value)

		return nil
	}

	f := reflect.ValueOf(dc).Elem().FieldByName(name)
	switch f.Kind() {

	case reflect.String:
		f.SetString(value)

	case reflect.Int32:
		// f represents a DomainEvent
		v, ok := domainEventNum[value]
		if !ok {
			return fmt.Errorf("'%v' is not a valid domain event spec", value)
		}
		f.SetInt(int64(v))

	default:
		// Cannot assign value to property
		return fmt.Errorf("cannot assign '%v' to property '%v' (type mismatch)", value, key)
	}

	return nil
}

// GetArrayProperty is like GetProperty, but requires an index and subkey as well. The index specifies
// which spec in the array to get, and the sub-key specifies the sub-property. Unlike GetProperty, GetArrayProperty
// will return at most one matching property.
func (d *Domain) GetArrayProperty(key string, index int, subKey string) (string, error) {
	dc := d.Config

	// Get the field name from xl key
	name, ok := fieldNameFromXLKey[key]
	if !ok {
		return "", fmt.Errorf("%v is not a valid key", key)
	}

	f := reflect.ValueOf(dc).Elem().FieldByName(name)
	if f.Kind() != reflect.Slice {
		return "", fmt.Errorf("cannot call GetArrayProperty on non-array type %v", key)
	}

	if index < 0 || index >= f.Len() {
		return "", fmt.Errorf("property '%v' with index %v does not exist", key, index)
	}

	var subName string
	switch key {
	case "vif":
		subName, ok = vifFieldNameFromKey[subKey]
		if !ok {
			return "", fmt.Errorf("%v is not a valid vif property", subKey)
		}

	case "pci":
		subName, ok = pciFieldNameFromKey[subKey]
		if !ok {
			return "", fmt.Errorf("%v is not a valid PCI property", subKey)
		}

	case "disk":
		subName, ok = diskFieldNameFromKey[subKey]
		if !ok {
			return "", fmt.Errorf("%v is not a valid disk property", subKey)
		}
	case "usbctrl":
		subName, ok = usbctrlFieldNameFromKey[subKey]
		if !ok {
			return "", fmt.Errorf("%v is not a valid usb controller property", subKey)
		}

	default:
		return "", fmt.Errorf("unexpected error: '%v' is a valid key but could not be handled", key)
	}

	// It is now safe to access underlying value by index and then field name
	v := f.Index(index).Elem().FieldByName(subName).Interface()

	return v.(string), nil
}

// SetArrayProperty is like SetProperty, but requires an index and subkey as well. The index specifies
// which spec in the array to modify, and the sub-key specifies the sub-property.
func (d *Domain) SetArrayProperty(key string, index int, subKey string, value string) error {
	dc := d.Config

	// Get the field name to set
	name, ok := fieldNameFromXLKey[key]
	if !ok {
		return fmt.Errorf("%v is not a valid key", key)
	}

	f := reflect.ValueOf(dc).Elem().FieldByName(name)
	if f.Kind() != reflect.Slice {
		return fmt.Errorf("cannot assign subkey to non-slice property %v", key)
	}

	switch key {
	case "vif":
		err := d.setNetworkProperty(index, subKey, value)
		if err != nil {
			return err
		}

	case "pci":
		err := d.setPCIProperty(index, subKey, value)
		if err != nil {
			return err
		}
	case "disk":
		err := d.setDiskProperty(index, subKey, value)
		if err != nil {
			return err
		}
	case "usbctrl":
		err := d.setUSBControllerProperty(index, subKey, value)
		if err != nil {
			return err
		}
	}

	return nil
}

func (d *Domain) setDiskProperty(index int, key string, value string) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Disks) {
		return fmt.Errorf("property 'disk' with index %v does not exist", index)
	}

	dd := dc.Disks[index]

	name, ok := diskFieldNameFromKey[key]
	if !ok {
		return fmt.Errorf("property 'disk' has no sub-property %v", key)
	}

	f := reflect.ValueOf(dd).Elem().FieldByName(name)
	if f.Kind() != reflect.String {
		return fmt.Errorf("cannot assign string '%v' to non-string property '%v'", value, key)
	}

	// Safe to assign a string now
	f.SetString(value)

	return nil
}

func (d *Domain) setNetworkProperty(index int, key string, value string) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Networks) {
		return fmt.Errorf("property 'vif' with index %v does not exist", index)
	}

	dn := dc.Networks[index]

	name, ok := vifFieldNameFromKey[key]
	if !ok {
		return fmt.Errorf("property 'vif' has no sub-property %v", key)
	}

	f := reflect.ValueOf(dn).Elem().FieldByName(name)
	if f.Kind() != reflect.String {
		return fmt.Errorf("cannot assign string '%v' to non-string property '%v'", value, key)
	}

	// Safe to assign a string now
	f.SetString(value)

	return nil
}

func (d *Domain) setPCIProperty(index int, key string, value string) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Pcidevs) {
		return fmt.Errorf("property 'pci' with index %v does not exist", index)
	}

	dp := dc.Pcidevs[index]

	name, ok := pciFieldNameFromKey[key]
	if !ok {
		return fmt.Errorf("property 'pci' has no sub-property %v", key)
	}

	f := reflect.ValueOf(dp).Elem().FieldByName(name)
	if f.Kind() != reflect.String {
		return fmt.Errorf("cannot assign string '%v' to non-string property '%v'", value, key)
	}

	// Safe to assign a string now
	f.SetString(value)

	return nil
}

func (d *Domain) setUSBControllerProperty(index int, key string, value string) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Usbctrls) {
		return fmt.Errorf("property 'usbctrl' with index %v does not exist", index)
	}

	duc := dc.Usbctrls[index]

	name, ok := usbctrlFieldNameFromKey[key]
	if !ok {
		return fmt.Errorf("property 'usbctrl' has no sub-property %v", key)
	}

	f := reflect.ValueOf(duc).Elem().FieldByName(name)
	if f.Kind() != reflect.String {
		return fmt.Errorf("cannot assign string '%v' to non-string property '%v'", value, key)
	}

	// Safe to assign a string now
	f.SetString(value)

	return nil
}

// AddNetwork appends 'dn' to the slice of DomainNetwork in 'dc'.
func (d *Domain) AddNetwork(dn *api.DomainNetwork) {
	dc := d.Config

	dc.Networks = append(dc.Networks, dn)
}

// RemoveNetwork removes the the DomainNetwork at a specified index from the DomainConfiguration. An error
// is returned if index is not valid.
func (d *Domain) RemoveNetwork(index int) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Networks) {
		return fmt.Errorf("network interface with index %v does not exist", index)
	}

	dc.Networks = append(dc.Networks[:index], dc.Networks[index+1:]...)

	return nil
}

// AddDisk appends 'dd' to the slice of DomainDisk in 'dc'.
func (d *Domain) AddDisk(dd *api.DomainDisk) {
	dc := d.Config

	dc.Disks = append(dc.Disks, dd)
}

// RemoveDisk removes the the DomainDisk at a specified index from the DomainConfiguration. An error
// is returned if index is not valid.
func (d *Domain) RemoveDisk(index int) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Disks) {
		return fmt.Errorf("disk with index %v does not exist", index)
	}

	dc.Disks = append(dc.Disks[:index], dc.Disks[index+1:]...)

	return nil
}

// AddPCIDevice appends 'd' to the slice of DomainPciDevice in 'dc'.
func (d *Domain) AddPCIDevice(dp *api.DomainPciDevice) {
	dc := d.Config

	dc.Pcidevs = append(dc.Pcidevs, dp)
}

// RemovePCIDevice removes the the DomainPCIDevice at a specified index from the DomainConfiguration. An error
// is returned if index is not valid.
func (d *Domain) RemovePCIDevice(index int) error {
	dc := d.Config

	if index < 0 || index >= len(dc.Pcidevs) {
		return fmt.Errorf("PCI device with index %v does not exist", index)
	}

	dc.Pcidevs = append(dc.Pcidevs[:index], dc.Pcidevs[index+1:]...)

	return nil
}
