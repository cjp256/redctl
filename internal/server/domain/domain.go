// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package domain

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"

	"gitlab.com/redfield/redctl/api"
)

// Domain struct
type Domain struct {
	*api.Domain
}

// NewDomain returns a new Domain, created from
// the specified api.Domain.
func NewDomain(d *api.Domain) *Domain {
	return &Domain{d}
}

func (d *Domain) writeConfigFile(path string) error {
	data, err := d.GenerateXlConfig()
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, data, 0644)
}

// Start starts a Domain
func (d *Domain) Start() error {
	path := fmt.Sprintf("/tmp/xl-%v", d.GetId().GetUuid())
	if err := d.writeConfigFile(path); err != nil {
		return err
	}

	if err := exec.Command("xl", "create", path).Run(); err != nil {
		return err
	}

	return nil
}

// Stop stops a domain
func (d *Domain) Stop() error {
	return exec.Command("xl", "shutdown", d.GetId().GetName()).Run()
}

// Restart restarts a domain
func (d *Domain) Restart() error {
	return exec.Command("xl", "reboot", d.GetId().GetName()).Run()
}

// HotplugNetworkAttach attaches a network to a domain
func (d *Domain) HotplugNetworkAttach(network api.DomainNetwork) error {
	// Replace the commas with spaces from MarshalText
	text, err := network.MarshalText()
	if err != nil {
		return err
	}
	// The -1 argument indicates that all commas should be replaced
	args := strings.Replace(string(text[:]), ",", " ", -1)

	return exec.Command("xl", "network-attach", args).Run()
}

// HotplugNetworkDetach detaches a network from a domain
func (d *Domain) HotplugNetworkDetach(network api.DomainNetwork) error {
	name := d.GetId().GetName()

	if devid := network.GetDevid(); devid != "" {
		return exec.Command("xl", "network-detach", name, devid).Run()
	}

	if mac := network.GetMac(); mac != "" {
		return exec.Command("xl", "network-detach", name, mac).Run()
	}

	// Either a devid or mac address is required
	return errors.New("either devid or mac address must be specified")
}

// HotplugPCIAttach attaches a PCI device to a domain
func (d *Domain) HotplugPCIAttach(pci api.DomainPciDevice) error {
	if pci.GetAddress() == "" {
		return errors.New("pci device address is required")
	}
	name := d.GetId().GetName()
	cmd := exec.Command("xl", "pci-attach", name, pci.GetAddress(), pci.GetVslot())

	return cmd.Run()
}

// HotplugPCIDetach detaches a PCI device from a domain
func (d *Domain) HotplugPCIDetach(pci api.DomainPciDevice) error {
	if pci.GetAddress() == "" {
		return errors.New("pci device address is required")
	}
	name := d.GetId().GetName()

	return exec.Command("xl", "pci-detach", name, pci.GetAddress()).Run()
}
