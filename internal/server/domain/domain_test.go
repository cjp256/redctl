// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package domain

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/redfield/redctl/test"
)

func createTempFile(t *testing.T, data []byte) string {
	tmpfile, err := ioutil.TempFile("", "test-xl-")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v\n", err)
	}

	if len(data) > 0 {
		err := ioutil.WriteFile(tmpfile.Name(), data, 0644)
		if err != nil {
			t.Fatalf("Failed to write temp file: %v\n", err)
		}
	}

	return tmpfile.Name()
}

func TestCreate(t *testing.T) {
	dir, err := ioutil.TempDir("", "create")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	t.Log("Setting up server...")
	test.SetupServer(t)
	defer test.Cleanup(t)

	t.Log("Setting up client...")
	client := test.SetupClient(t)

	xlcfg := `
	name="TestName"
	memory="1024"
	`

	tf := createTempFile(t, []byte(xlcfg))
	defer os.Remove(tf)

	d := NewDomain().
		WithDomainCachePath(dir).
		WithName("TestName").
		WithDescription("test description")

	if err := d.ImportXlConfiguration(tf); err != nil {
		t.Fatalf("Failed to import xl configuration: %v\n", err)
	}

	if err := d.Create(client); err != nil {
		t.Errorf("Failed to create domain: %v\n", err)
	}

	d2 := NewDomain()
	if err := d2.Create(client); err != nil {
		t.Errorf("Failed to create domain: %s", err.Error())
	}
}
