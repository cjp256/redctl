// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package domain

import (
	"bytes"

	"github.com/BurntSushi/toml"

	"gitlab.com/redfield/redctl/api"
)

var (
	// Coversion between DomainEvent type and string expected for xl.cfg.
	domainEventString = map[api.DomainEvent]string{
		api.DomainEvent_DOMAIN_EVENT_UNSPECIFIED:      "",
		api.DomainEvent_DOMAIN_EVENT_DESTROY:          "destroy",
		api.DomainEvent_DOMAIN_EVENT_RESTART:          "restart",
		api.DomainEvent_DOMAIN_EVENT_RENAME_RESTART:   "rename-restart",
		api.DomainEvent_DOMAIN_EVENT_PRESERVE:         "preserve",
		api.DomainEvent_DOMAIN_EVENT_COREDUMP_DESTROY: "coredump-destroy",
		api.DomainEvent_DOMAIN_EVENT_COREDUMP_RESTART: "coredump-restart",
		api.DomainEvent_DOMAIN_EVENT_SOFT_RESET:       "soft-reset",
	}

	domainEventNum = map[string]api.DomainEvent{
		"":                 api.DomainEvent_DOMAIN_EVENT_UNSPECIFIED,
		"destroy":          api.DomainEvent_DOMAIN_EVENT_DESTROY,
		"restart":          api.DomainEvent_DOMAIN_EVENT_RESTART,
		"rename-restart":   api.DomainEvent_DOMAIN_EVENT_RENAME_RESTART,
		"preserve":         api.DomainEvent_DOMAIN_EVENT_PRESERVE,
		"coredump-destroy": api.DomainEvent_DOMAIN_EVENT_COREDUMP_DESTROY,
		"coredump-restart": api.DomainEvent_DOMAIN_EVENT_COREDUMP_RESTART,
		"soft-reset":       api.DomainEvent_DOMAIN_EVENT_SOFT_RESET,
	}

	// Map xl keys to DomainConfiguration field names
	fieldNameFromXLKey = map[string]string{
		"name":              "Name",
		"type":              "Type",
		"vcpus":             "Vcpus",
		"memory":            "Memory",
		"kernel":            "Kernel",
		"ramdisk":           "Ramdisk",
		"cmdline":           "Cmdline",
		"boot":              "Boot",
		"usb":               "Usb",
		"usbdevice":         "Usbdevice",
		"usbctrl":           "Usbctrls",
		"vga":               "Vga",
		"sdl":               "Sdl",
		"vnc":               "Vnc",
		"soundhw":           "Soundhw",
		"disk":              "Disks",
		"vif":               "Networks",
		"pci":               "Pcidevs",
		"device_model_args": "DeviceModelArgs",
		"on_poweroff":       "OnPoweroff",
		"on_reboot":         "OnReboot",
		"on_watchdog":       "OnWatchdog",
		"on_crash":          "OnCrash",
		"on_soft_reset":     "OnSoftReset",
	}

	pciFieldNameFromKey = map[string]string{
		"address":     "Address",
		"vslot":       "Vslot",
		"permissive":  "Permissive",
		"sitranslate": "Msitranslate",
		"seize":       "Seize",
		"power_mgmt":  "PowerMgmt",
		"rdm_policy":  "RdmPolicy",
	}

	vifFieldNameFromKey = map[string]string{
		"mac":        "Mac",
		"bridge":     "Bridge",
		"gatewaydev": "Gatewaydev",
		"type":       "Type",
		"model":      "Model",
		"vifname":    "Vifname",
		"script":     "Script",
		"ip":         "Ip",
		"backend":    "Backend",
		"rate":       "Rate",
		"devid":      "Devid",
	}

	diskFieldNameFromKey = map[string]string{
		"target":      "Target",
		"format":      "Format",
		"vdev":        "Vdev",
		"access":      "Access",
		"devtype":     "Devtype",
		"backend":     "Backend",
		"backendtype": "Backendtype",
		"script":      "Script",
		"discard":     "Discard",
	}

	usbctrlFieldNameFromKey = map[string]string{
		"type":    "Type",
		"version": "Version",
		"ports":   "Ports",
	}
)

// xlConfig represents an xl.cfg (https://xenbits.xen.org/docs/unstable/man/xl.cfg.5.html),
// and mirrors redfield api.DomainConfiguration in structure since
// gRPC does not allow custom tags, so XlConfig serves the purpose of providing TOML tags.
type xlConfig struct {
	Name      string `toml:"name,omitempty"`
	Type      string `toml:"type,omitempty"`
	VCPUs     string `toml:"vcpus,omitempty"`
	Memory    string `toml:"memory,omitempty"`
	Kernel    string `toml:"kernel,omitempty"`
	RamDisk   string `toml:"ramdisk,omitempty"`
	CmdLine   string `toml:"cmdline,omitempty"`
	Boot      string `toml:"boot,omitempty"`
	USB       string `toml:"usb,omitempty"`
	USBDevice string `toml:"usbdevice,omitempty"`
	VGA       string `toml:"vga,omitempty"`
	SDL       string `toml:"sdl,omitempty"`
	VNC       string `toml:"vnc,omitempty"`
	SoundHW   string `toml:"soundhw,omitempty"`

	Disks []*api.DomainDisk `toml:"disk,omitempty"`

	Networks []*api.DomainNetwork `toml:"vif,omitempty"`

	PCIDevices []*api.DomainPciDevice `toml:"pci,omitempty"`

	USBControllers []*api.DomainUsbController `toml:"usbctrl,omitempty"`

	DeviceModelArgs []string `toml:"device_model_args,omitempty"`

	OnPoweroff  string `toml:"on_poweroff,omitempty"`
	OnReboot    string `toml:"on_reboot,omitempty"`
	OnWatchdog  string `toml:"on_watchdog,omitempty"`
	OnCrash     string `toml:"on_crash,omitempty"`
	OnSoftReset string `toml:"on_soft_reset,omitempty"`
}

func (d *Domain) convertToXlConfig() *xlConfig {
	dc := d.Config

	cfg := xlConfig{
		Name:      dc.Name,
		Type:      dc.Type,
		VCPUs:     dc.Vcpus,
		Memory:    dc.Memory,
		Kernel:    dc.Kernel,
		RamDisk:   dc.Ramdisk,
		CmdLine:   dc.Cmdline,
		Boot:      dc.Boot,
		USB:       dc.Usb,
		USBDevice: dc.Usbdevice,
		VGA:       dc.Vga,
		SDL:       dc.Sdl,
		VNC:       dc.Vnc,
		SoundHW:   dc.Soundhw,

		Disks: dc.Disks,

		Networks: dc.Networks,

		PCIDevices: dc.Pcidevs,

		USBControllers: dc.Usbctrls,

		DeviceModelArgs: dc.DeviceModelArgs,

		// Use the string representation of the domain event
		OnPoweroff:  domainEventString[dc.OnPoweroff],
		OnReboot:    domainEventString[dc.OnReboot],
		OnWatchdog:  domainEventString[dc.OnWatchdog],
		OnCrash:     domainEventString[dc.OnCrash],
		OnSoftReset: domainEventString[dc.OnSoftReset],
	}

	return &cfg
}

// GenerateXlConfig creates an xl.cfg based on the fields of
// a Domain's DomainConfiguration. The config is given as a byte slice,
// and an error is returned if TOML encoding fails.
func (d *Domain) GenerateXlConfig() ([]byte, error) {
	cfg := d.convertToXlConfig()

	// Use bytes.Buffer, which implements io.Writer
	b := bytes.NewBuffer([]byte{})
	err := toml.NewEncoder(b).Encode(cfg)

	return b.Bytes(), err
}
