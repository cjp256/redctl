// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/google/uuid"

	"google.golang.org/grpc"

	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"

	"gitlab.com/redfield/redctl/api"
	"gitlab.com/redfield/redctl/internal/server/domain"
	"gitlab.com/redfield/redctl/internal/server/repository"
)

// server is used to implement redfield.RedctlServer.
type server struct {
	domainRepo repository.DomainRepo
	imageRepo  repository.ImageRepo
}

// unix sockets not compatible with TLS it appears
var secure bool = false

// PING

func (s *server) Ping(ctx context.Context, in *api.PingRequest) (*api.PingReply, error) {
	return &api.PingReply{Msg: in.Msg}, nil
}

// DOMAIN

func (s *server) DomainCreate(ctx context.Context, in *api.DomainCreateRequest) (*api.DomainCreateReply, error) {
	domain := *in.Domain
	id := &api.DomainId{
		Uuid: uuid.New().String(),
		Name: domain.GetId().GetName(),
	}

	domain.Id = id

	log.Printf("opened db collection - creating: %+v", domain)

	err := s.domainRepo.Create(&domain)
	if err != nil {
		log.Printf("failed to create domain: %v %v\n", domain, err)
		return nil, fmt.Errorf("failed to create domain: %v %v\n", domain, err)
	}

	log.Printf("created domain: %+v\n", domain)

	return &api.DomainCreateReply{Domain: &domain}, nil
}

func (s *server) queryDomains(id *api.DomainId) ([]*api.Domain, error) {
	var domains []*api.Domain

	domains, err := s.domainRepo.FindAll()
	if err != nil {
		return domains, err
	}

	uuid := id.GetUuid()
	name := id.GetName()
	for _, domain := range domains {
		dName := domain.GetId().GetName()
		dUUID := domain.GetId().GetUuid()
		if dName == name || dUUID == uuid {
			domains = append(domains, domain)
		}
	}

	return domains, nil
}

func (s *server) DomainFind(ctx context.Context, in *api.DomainFindRequest) (*api.DomainFindReply, error) {
	domains, err := s.queryDomains(in.GetId())
	if err != nil {
		log.Printf("failed to find results: %v %v\n", in.GetId(), err)
		return nil, fmt.Errorf("failed to find domain: %v %v\n", in.GetId(), err)
	}

	log.Printf("find domain: %+v\n", domains)

	return &api.DomainFindReply{Domains: domains}, nil
}

func (s *server) DomainUpdate(ctx context.Context, in *api.DomainUpdateRequest) (*api.DomainUpdateReply, error) {
	domain := *in.Domain

	log.Printf("opened db collection - updating: %+v", domain)

	err := s.domainRepo.Update(&domain)
	if err != nil {
		log.Printf("failed to update domain: %v %v\n", domain, err)
		return nil, fmt.Errorf("failed to update domain: %v %v\n", domain, err)
	}

	log.Printf("updated domain: %+v\n", domain)

	return &api.DomainUpdateReply{Domain: &domain}, nil
}

func (s *server) DomainRemove(ctx context.Context, in *api.DomainRemoveRequest) (*api.DomainRemoveReply, error) {
	id := in.Uuid

	log.Printf("opened db collection - removing: %v", id)

	err := s.domainRepo.Remove(id)
	if err != nil {
		log.Printf("failed to remove domain: %v %v\n", id, err)
		return nil, fmt.Errorf("failed to remove domain: %v %v\n", id, err)
	}

	log.Printf("removed domain: %v\n", id)

	return &api.DomainRemoveReply{}, nil
}

func (s *server) DomainStart(ctx context.Context, in *api.DomainStartRequest) (*api.DomainStartReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to start domain: %v", err)
	}

	d := domain.NewDomain(record)
	if err := d.Start(); err != nil {
		return nil, fmt.Errorf("unable to start domain: %v", err)
	}

	return &api.DomainStartReply{}, nil
}

func (s *server) DomainStop(ctx context.Context, in *api.DomainStopRequest) (*api.DomainStopReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to stop domain: %v", err)
	}

	d := domain.NewDomain(record)
	if err := d.Stop(); err != nil {
		return nil, fmt.Errorf("unable to stop domain: %v", err)
	}

	return &api.DomainStopReply{}, nil
}

func (s *server) DomainRestart(ctx context.Context, in *api.DomainRestartRequest) (*api.DomainRestartReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to restart domain: %v", err)
	}

	d := domain.NewDomain(record)
	if err := d.Restart(); err != nil {
		return nil, fmt.Errorf("unable to restart domain: %v", err)
	}

	return &api.DomainRestartReply{}, nil
}

func (s *server) DomainHotplugNetworkAttach(ctx context.Context, in *api.DomainHotplugNetworkAttachRequest) (*api.DomainHotplugNetworkAttachReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to attach network to domain: %v", err)
	}

	d := domain.NewDomain(record)
	n := in.GetNetwork()

	if err := d.HotplugNetworkAttach(*n); err != nil {
		return nil, fmt.Errorf("unable to attach network to domain: %v", err)
	}

	return &api.DomainHotplugNetworkAttachReply{}, nil
}

func (s *server) DomainHotplugNetworkDetach(ctx context.Context, in *api.DomainHotplugNetworkDetachRequest) (*api.DomainHotplugNetworkDetachReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to attach network to domain: %v", err)
	}

	d := domain.NewDomain(record)
	n := in.GetNetwork()

	if err := d.HotplugNetworkDetach(*n); err != nil {
		return nil, fmt.Errorf("unable to attach network to domain: %v", err)
	}

	return &api.DomainHotplugNetworkDetachReply{}, nil
}

func (s *server) DomainHotplugPciAttach(ctx context.Context, in *api.DomainHotplugPciAttachRequest) (*api.DomainHotplugPciAttachReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	d := domain.NewDomain(record)
	p := in.GetPcidev()

	if err := d.HotplugPCIAttach(*p); err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	return &api.DomainHotplugPciAttachReply{}, nil
}

func (s *server) DomainHotplugPciDetach(ctx context.Context, in *api.DomainHotplugPciDetachRequest) (*api.DomainHotplugPciDetachReply, error) {
	record, err := s.domainRepo.Find(in.GetId().GetUuid())
	if err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	d := domain.NewDomain(record)
	p := in.GetPcidev()

	if err := d.HotplugPCIDetach(*p); err != nil {
		return nil, fmt.Errorf("unable to attach pci device to domain: %v", err)
	}

	return &api.DomainHotplugPciDetachReply{}, nil
}

// IMAGE

func (s *server) ImageCreate(ctx context.Context, in *api.ImageCreateRequest) (*api.ImageCreateReply, error) {
	image := *in.Image
	image.Uuid = uuid.New().String()

	err := s.imageRepo.Create(&image)
	if err != nil {
		log.Printf("failed to create image: %v %v\n", image, err)
		return nil, fmt.Errorf("failed to create image: %v %v\n", image, err)
	}

	log.Printf("created image: %+v\n", image)

	return &api.ImageCreateReply{Image: &image}, nil
}

func (s *server) ImageFind(ctx context.Context, in *api.ImageFindRequest) (*api.ImageFindReply, error) {
	var images []*api.Image

	images, err := s.imageRepo.FindAll()
	if err != nil {
		log.Printf("failed to find results: %v %v\n", in, err)
		return nil, fmt.Errorf("failed to find image: %v %v\n", in, err)
	}

	uuid := in.GetUuid()
	name := in.GetName()
	for _, image := range images {
		iName := image.GetName()
		iUUID := image.GetUuid()
		if iName == name || iUUID == uuid {
			images = append(images, image)
		}
	}

	log.Printf("find image: %+v\n", images)

	return &api.ImageFindReply{Images: images}, nil
}

func (s *server) ImageUpdate(ctx context.Context, in *api.ImageUpdateRequest) (*api.ImageUpdateReply, error) {
	image := *in.Image

	err := s.imageRepo.Update(&image)
	if err != nil {
		log.Printf("failed to update image: %v %v\n", image, err)
		return nil, fmt.Errorf("failed to update image: %v %v\n", image, err)
	}

	log.Printf("updated image: %+v\n", image)

	return &api.ImageUpdateReply{Image: &image}, nil
}

func (s *server) ImageRemove(ctx context.Context, in *api.ImageRemoveRequest) (*api.ImageRemoveReply, error) {
	id := in.Uuid

	err := s.imageRepo.Remove(id)
	if err != nil {
		log.Printf("failed to remove image: %v %v\n", id, err)
		return nil, fmt.Errorf("failed to remove image: %v %v\n", id, err)
	}

	log.Printf("removed image: %v\n", id)

	return &api.ImageRemoveReply{}, nil
}

func grpcAuthenticateClient(ctx context.Context) (string, error) {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		username := strings.Join(md["username"], "")
		password := strings.Join(md["password"], "")

		if !authenticateUser(username, password) {
			return "", fmt.Errorf("bad auth: %s %s", username, password)
		}

		log.Printf("authenticated client: %s", username)
		return username, nil
	}

	return "", fmt.Errorf("missing credentials")
}

func grpcUnaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	_, err := grpcAuthenticateClient(ctx)
	if err != nil {
		return nil, err
	}

	return handler(ctx, req)
}

// GrpcServe initializes and serves gRPC with specified server options (TLS, etc.)
func GrpcServe(ln net.Listener, dr repository.DomainRepo, ir repository.ImageRepo, opts *RedctlServerOptions) error {
	var gopts []grpc.ServerOption

	srv := server{domainRepo: dr, imageRepo: ir}

	if secure {
		creds, err := credentials.NewServerTLSFromFile(opts.TlsCertFile, opts.TlsKeyFile)
		if err != nil {
			log.Printf("Failed to generate credentials %v", err)
			return err
		}

		gopts = append(gopts, grpc.Creds(creds))
		gopts = append(gopts, grpc.UnaryInterceptor(grpcUnaryInterceptor))
	}

	// Setup grpc server
	s := grpc.NewServer(gopts...)
	api.RegisterRedctlServer(s, &srv)
	reflection.Register(s)
	return s.Serve(ln)
}
