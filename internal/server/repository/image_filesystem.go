// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"gitlab.com/redfield/redctl/api"
)

type filesystemImageRepo struct {
	path string
}

// NewFilesystemImageRepo for repository resident in filesystem (images path)
func NewFilesystemImageRepo(path string) ImageRepo {
	return &filesystemImageRepo{path: path}
}

func (s *filesystemImageRepo) Find(uuid string) (*api.Image, error) {
	return nil, nil
}

func (s *filesystemImageRepo) FindAll() ([]*api.Image, error) {
	return nil, nil
}

func (s *filesystemImageRepo) Create(image *api.Image) error {
	return nil
}

func (s *filesystemImageRepo) Update(image *api.Image) error {
	return nil
}

func (s *filesystemImageRepo) Remove(uuid string) error {
	return nil
}
