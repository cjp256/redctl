// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"gitlab.com/redfield/redctl/api"
)

// ImageRepo interface
type ImageRepo interface {
	// Create image
	Create(domain *api.Image) error

	// Find image
	Find(uuid string) (*api.Image, error)

	// Find all images
	FindAll() ([]*api.Image, error)

	// Remove image
	Remove(uuid string) error

	// Update existing image
	Update(domain *api.Image) error
}
