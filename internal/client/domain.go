// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"errors"
	"fmt"

	"gitlab.com/redfield/redctl/api"
)

// NewUserDomain creates a new domain from a template
// for a user domain.
func NewUserDomain() *api.Domain {
	return &api.Domain{
		Config: &api.DomainConfiguration{
			Type:    "hvm",
			Vcpus:   "4",
			Memory:  "4096",
			Usb:     "1",
			Vga:     "stdvga",
			Sdl:     "0",
			Vnc:     "0",
			Soundhw: "hda",

			DeviceModelArgs: []string{"-display", "gtk,gl=on", "-full-screen"},

			Usbctrls: []*api.DomainUsbController{
				{Type: "devicemodel", Version: "1"},
				{Type: "devicemodel", Version: "2"},
				{Type: "devicemodel", Version: "3"},
			},
		},
	}
}

// NewPVDomain creates a new domain from a template for a
// PV domain.
func NewPVDomain() *api.Domain {
	return &api.Domain{
		Config: &api.DomainConfiguration{
			Type:    "pv",
			Vcpus:   "2",
			Memory:  "1024",
			Kernel:  "/boot/bzImage",
			Cmdline: "ignore_logelevel boot=/dev/xvda",
		},
		StartOnBoot: true,
	}
}

// NewPVHDomain creates a new domain from a template for a
// PVH domain.
func NewPVHDomain() *api.Domain {
	d := NewPVDomain()
	d.Config.Type = "pvh"
	return d
}

// DomainStart to start domain
func (c *Client) DomainStart(uuid, name string) error {
	if uuid == "" && name == "" {
		return errors.New("domain name or uuid required")
	}

	r := api.DomainStartRequest{
		Id: &api.DomainId{Uuid: uuid, Name: name},
	}
	_, err := c.redctlClient.DomainStart(c.Context(), &r)

	return err
}

// DomainCreate to create new domain
func (c *Client) DomainCreate(dom *api.Domain) (*api.Domain, error) {
	fmt.Println(dom)

	r := api.DomainCreateRequest{Domain: dom}
	d, err := c.redctlClient.DomainCreate(c.Context(), &r)
	if err != nil {
		return dom, err
	}

	return d.GetDomain(), nil
}
