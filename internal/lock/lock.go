// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lock

import (
	"github.com/gofrs/flock"
)

var lockFile *flock.Flock

func initLock(path string) {
	// if lock already initialized, return
	if lockFile != nil {
		return
	}

	// default lock path
	if path == "" {
		path = "/var/lock/api.lock"
	}

	lockFile = flock.New(path)
}

// TryLock attempts lock if available, otherwise returns
func TryLock() (bool, error) {
	initLock("")
	return lockFile.TryLock()
}

// Locked checks to see if locked
func Locked() bool {
	initLock("")
	return lockFile.Locked()
}

// Lock redctl, blocking indefinitely if desired
// Returns whether lock was taken, error if any
func Lock(block bool) (bool, error) {
	initLock("")

	if block {
		return true, lockFile.Lock()
	}

	return lockFile.TryLock()
}

// Unlock redctl
func Unlock() error {
	return lockFile.Unlock()
}
