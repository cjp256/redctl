// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// luksCmd represents the luks command
var (
	luksCmd = &cobra.Command{
		Use:   "luks",
		Short: "Manage LUKS software disk encryption",
		Long:  `Assign, query, and remove key slots`,
	}
)

var (
	luksSlot int

	luksAddKeyCmd = &cobra.Command{
		Use:   "add-key",
		Short: "Add LUKS key to slot",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks add-key called")
		},
	}

	luksRemoveKeyCmd = &cobra.Command{
		Use:   "remove-key",
		Short: "Remove LUKS key",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks remove-key called")
		},
	}

	luksQuerySlotsCmd = &cobra.Command{
		Use:   "query-slots",
		Short: "Query slot info",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("luks query-slots called")
		},
	}
)

func init() {
	rootCmd.AddCommand(luksCmd)
	luksCmd.AddCommand(luksAddKeyCmd)
	luksCmd.AddCommand(luksRemoveKeyCmd)
	luksCmd.AddCommand(luksQuerySlotsCmd)

	luksCmd.PersistentFlags().IntVar(&luksSlot, "slot", 0, "luks slot index")
}
