// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	imageUuid string

	imageCmd = &cobra.Command{
		Use:   "image",
		Short: "Manage virtual disk images",
		Long:  ``,
	}
)

var (
	imageCreateSize   string
	imageCreateFormat string
	imageCreateName   string

	imageCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create empty virtual disk image",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image create called")
		},
	}
)

var (
	imageDownloadPath string

	imageDownloadCmd = &cobra.Command{
		Use:   "download",
		Short: "Download image from disk repository",
		Long:  `Synchronize disks from repository`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image download called")
		},
	}
)

var (
	imageUploadPath string

	imageUploadCmd = &cobra.Command{
		Use:   "upload",
		Short: "Upload image to disk repository",
		Long:  `Synchronize disks up to repository`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image upload called")
		},
	}
)

var (
	imageRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove local or remote image",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image remove called")
		},
	}
)

func init() {
	rootCmd.AddCommand(imageCmd)
	imageCmd.AddCommand(imageRemoveCmd)
	imageCmd.AddCommand(imageCreateCmd)
	imageCmd.AddCommand(imageDownloadCmd)
	imageCmd.AddCommand(imageUploadCmd)

	imageCreateCmd.PersistentFlags().StringVar(&imageCreateSize, "size", "80G", "Size (common suffixes, e.g. k, m, g)")
	imageCreateCmd.PersistentFlags().StringVar(&imageCreateFormat, "format", "qcow2", "Format [qcow2|raw]")
	imageCreateCmd.PersistentFlags().StringVar(&imageCreateName, "name", "", "Image name")
	imageDownloadCmd.PersistentFlags().StringVar(&imageDownloadPath, "path", "", "Output file path (if no uuid specified)")
	imageUploadCmd.PersistentFlags().StringVar(&imageUploadPath, "path", "", "Input file path (if no uuid specified)")

}
