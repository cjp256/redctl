// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/viper"

	"gitlab.com/redfield/redctl/internal/client"
)

func initializeRedctlClient() *client.Client {
	c := client.NewClient().
		WithAuthUser(viper.GetString("username")).
		WithAuthPassword(viper.GetString("password")).
		WithServerUrl(viper.GetString("server-url")).
		WithTimeout(viper.GetString("timeout")).
		WithTlsCaCertFile(viper.GetString("tls-ca-certificate")).
		WithTlsCertFile(viper.GetString("tls-certificate")).
		WithTlsKeyFile(viper.GetString("tls-key"))

	// if local mode, don't dial
	if viper.GetBool("local") == true {
		return c
	}

	if err := c.Dial(); err != nil {
		log.Fatalln("Failed to dial server:", err)
		return nil
	}

	return c
}
