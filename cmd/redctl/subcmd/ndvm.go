// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// ndvmCmd represents the ndvm command
var (
	ndvmUuid string
	ndvmName string

	ndvmCmd = &cobra.Command{
		Use:   "ndvm",
		Short: "Manage network virtual machines (NDVMs)",
		Long:  ``,
	}
)

var (
	ndvmListCmd = &cobra.Command{
		Use:   "list",
		Short: "List network domain VMs",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ndvm list called")
		},
	}
)

var (
	ndvmPostInstallConfigPath string
	ndvmPostInstallConfig     string
	ndvmBaseImagePath         string

	ndvmPostInstallCmd = &cobra.Command{
		Use:   "post-install",
		Short: "Execute post-install actions from base rootfs",
		Long:  `Regenerates disk image based on non-volatile configuration`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ndvm post-install called")
		},
	}
)

var (
	ndvmBridgeName       string
	ndvmBridgeMaxClients int

	ndvmRegisterBridgeCmd = &cobra.Command{
		Use:   "register-bridge",
		Short: "Advertise bridge exported by network vm",
		Long:  `Set bridge name and specify number of clients supports`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ndvm register-bridge called")
		},
	}

	ndvmUnregisterBridgeCmd = &cobra.Command{
		Use:   "unregister-bridge",
		Short: "Remove advertised bridge exported by network vm",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("ndvm unregister-bridge called")
		},
	}
)

func init() {
	rootCmd.AddCommand(ndvmCmd)
	ndvmCmd.PersistentFlags().StringVar(&ndvmUuid, "uuid", "", "vm uuid")
	ndvmCmd.PersistentFlags().StringVar(&ndvmName, "name", "", "vm name")

	ndvmCmd.AddCommand(ndvmListCmd)

	ndvmCmd.AddCommand(ndvmPostInstallCmd)
	ndvmCmd.AddCommand(ndvmRegisterBridgeCmd)
	ndvmPostInstallCmd.PersistentFlags().StringVar(&ndvmPostInstallConfigPath, "config-path", "", "path to config file used by network vm")
	ndvmPostInstallCmd.PersistentFlags().StringVar(&ndvmPostInstallConfig, "config", "", "config string used by network vm")

	ndvmCmd.AddCommand(ndvmUnregisterBridgeCmd)
	ndvmRegisterBridgeCmd.PersistentFlags().StringVar(&ndvmBridgeName, "bridge-name", "", "name of bridge")
	ndvmUnregisterBridgeCmd.PersistentFlags().StringVar(&ndvmBridgeName, "bridge-name", "", "name of bridge")
	ndvmRegisterBridgeCmd.PersistentFlags().IntVar(&ndvmBridgeMaxClients, "max-clients", 0, "max number of clients supported by bridge")

}
