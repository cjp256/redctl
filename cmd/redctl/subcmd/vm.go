// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/redctl/internal/client"
	"gitlab.com/redfield/redctl/internal/lock"
)

// vmCmd represents the vm command
var (
	vmCmd = &cobra.Command{
		Use:   "vm",
		Short: "Manage virtual machines",
		Long:  `VM Creation, Copying, Modification, Deletion`,
	}
)

var (
	vmCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create user virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("creating vm...")
			rc := initializeRedctlClient()
			defer rc.Close()

			if _, err := lock.Lock(true); err != nil {
				log.Fatalln("Failed to take redctl lock:", err)
			}
			defer lock.Unlock()

			d, err := rc.DomainCreate(client.NewUserDomain())
			if err != nil {
				log.Fatalf("Failed to create domain: %v", err)
			}

			log.Print(d)
		},
	}
)

var (
	vmGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm get called")
		},
	}

	vmSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm set called")
		},
	}
)

var (
	vmRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove user virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm remove called")
		},
	}
)

var (
	vmNetworkCmd = &cobra.Command{
		Use:   "network",
		Short: "Manage virtual machine virtual interfaces (subcommand)",
		Long:  `VM Virtual Network Interface Creation, Copying, Modification, Deletion`,
	}

	vmNetworkListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual network interfaces",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm network list called")
		},
	}

	vmNetworkCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm network create called")
		},
	}

	vmNetworkGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm network get called")
		},
	}

	vmNetworkSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm network set called")
		},
	}

	vmNetworkRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove virtual network interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm network remove called")
		},
	}
)

var (
	vmDiskCmd = &cobra.Command{
		Use:   "disk",
		Short: "Manage virtual machine virtual interfaces (subcommand)",
		Long:  `VM Virtual Disk Interface Creation, Copying, Modification, Deletion`,
	}

	vmDiskListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual disk interfaces",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm disk list called")
		},
	}

	vmDiskCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create virtual disk interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm disk create called")
		},
	}

	vmDiskGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm disk get called")
		},
	}

	vmDiskSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm disk set called")
		},
	}

	vmDiskRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove virtual disk interface",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm disk remove called")
		},
	}
)

var (
	vmPciCmd = &cobra.Command{
		Use:   "pci",
		Short: "Manage virtual machine PCI pass-through devices (subcommand)",
		Long:  `VM Virtual Pci Interface Assignment`,
	}

	vmPciListCmd = &cobra.Command{
		Use:   "list",
		Short: "List pci pass-through devices",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm pci list called")
		},
	}

	vmPciAssignCmd = &cobra.Command{
		Use:   "assign",
		Short: "Assign pci pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm pci assign called")
		},
	}

	vmPciGetCmd = &cobra.Command{
		Use:   "get-property",
		Short: "Get key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm pci get called")
		},
	}

	vmPciSetCmd = &cobra.Command{
		Use:   "set-property",
		Short: "Set key property-value",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm pci set called")
		},
	}

	vmPciRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove PCI pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm pci remove called")
		},
	}
)

var (
	vmUsbCmd = &cobra.Command{
		Use:   "usb",
		Short: "Manage virtual machine USB pass-through devices (subcommand)",
		Long:  `VM Virtual USB Interface Assignment`,
	}

	vmUsbListCmd = &cobra.Command{
		Use:   "list",
		Short: "List usb pass-through devices",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm usb list called")
		},
	}

	vmUsbAssignCmd = &cobra.Command{
		Use:   "assign",
		Short: "Assign usb pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm usb assign called")
		},
	}

	vmUsbRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove usb pass-through device",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm usb remove called")
		},
	}
)

func init() {
	rootCmd.AddCommand(vmCmd)
	vmCmd.PersistentFlags().String("uuid", "", "vm uuid")
	vmCmd.PersistentFlags().String("name", "", "vm name")

	vmCmd.AddCommand(vmCreateCmd)
	vmCreateCmd.PersistentFlags().String("xl-config", "", "Optional xl config to import as template")
	vmCreateCmd.PersistentFlags().String("description", "", "Optional vm description")

	vmCmd.AddCommand(vmRemoveCmd)

	vmCmd.AddCommand(vmGetCmd)
	vmGetCmd.PersistentFlags().String("key", "", "property key")

	vmCmd.AddCommand(vmSetCmd)
	vmSetCmd.PersistentFlags().String("key", "", "property key")
	vmSetCmd.PersistentFlags().String("value", "", "property value")

	vmCmd.AddCommand(vmNetworkCmd)
	vmNetworkCmd.AddCommand(vmNetworkListCmd)
	vmNetworkCmd.AddCommand(vmNetworkCreateCmd)
	vmNetworkCmd.AddCommand(vmNetworkGetCmd)
	vmNetworkCmd.AddCommand(vmNetworkSetCmd)
	vmNetworkCmd.AddCommand(vmNetworkRemoveCmd)
	vmNetworkCmd.PersistentFlags().Int("index", 0, "vm virtual network index")

	vmCmd.AddCommand(vmDiskCmd)
	vmDiskCmd.AddCommand(vmDiskListCmd)
	vmDiskCmd.AddCommand(vmDiskCreateCmd)
	vmDiskCmd.AddCommand(vmDiskGetCmd)
	vmDiskCmd.AddCommand(vmDiskSetCmd)
	vmDiskCmd.AddCommand(vmDiskRemoveCmd)
	vmDiskCmd.PersistentFlags().Int("index", 0, "vm virtual disk index")

	vmCmd.AddCommand(vmPciCmd)
	vmPciCmd.AddCommand(vmPciListCmd)
	vmPciCmd.AddCommand(vmPciAssignCmd)
	vmPciCmd.AddCommand(vmPciGetCmd)
	vmPciCmd.AddCommand(vmPciSetCmd)
	vmPciCmd.AddCommand(vmPciRemoveCmd)
	vmPciCmd.PersistentFlags().Int("index", 0, "vm virtual pci index")

	vmCmd.AddCommand(vmUsbCmd)
	vmUsbCmd.AddCommand(vmUsbListCmd)
	vmUsbCmd.AddCommand(vmUsbAssignCmd)
	vmUsbCmd.AddCommand(vmUsbRemoveCmd)
	vmUsbCmd.PersistentFlags().Int("index", 0, "vm virtual usb index")
	vmUsbCmd.PersistentFlags().String("device-id", "", "usb device id")
	vmUsbCmd.PersistentFlags().String("vendor-id", "", "usb vendor id")
	vmUsbCmd.PersistentFlags().String("bus-address", "", "usb device bus")
	vmUsbCmd.PersistentFlags().String("device-address", "", "usb vendor device address")
}
