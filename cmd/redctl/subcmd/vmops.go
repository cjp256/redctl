// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/redctl/internal/lock"
)

// opsCmd represents the vm command
var (
	vmOpsCmd = &cobra.Command{
		Use:   "vm-operations",
		Short: "Trigger local VM operations/events",
		Long:  `VM Start, Stop, Reboot, etc.`,
	}
)

var (
	vmOpsStartCmd = &cobra.Command{
		Use:   "start",
		Short: "Start virtual machine",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			viper.BindPFlags(cmd.PersistentFlags())
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("vm-operations start called")
			client := initializeRedctlClient()
			defer client.Close()

			if _, err := lock.Lock(true); err != nil {
				log.Fatalln("Failed to take redctl lock:", err)
			}
			defer lock.Unlock()

			name := viper.GetString("name")
			uuid := viper.GetString("uuid")

			if err := client.DomainStart(uuid, name); err != nil {
				log.Fatalf("Failed to start domain: %v", err)
			}
		},
	}
)

/*func getDomainByUuidOrName(cachePath string, uuid string, name string) *domain.Domain {
	var d *domain.Domain
	if uuid != "" {
		d = domain.NewDomainFromCacheByUuid(cachePath, uuid)
		if d == nil {
			log.Fatalln("Failed to find vm with uuid:", uuid)
		}
	} else if name != "" {
		d = domain.NewDomainFromCacheByName(cachePath, name)
		if d == nil {
			log.Fatalln("Failed to find vm:", name)
		}
	} else {
		log.Fatalln("Failed to find vm: no domain specified")
	}

	return d
}*/

func init() {
	rootCmd.AddCommand(vmOpsCmd)

	vmOpsCmd.AddCommand(vmOpsStartCmd)
	vmOpsStartCmd.PersistentFlags().String("name", "", "vm name")
	vmOpsStartCmd.PersistentFlags().String("uuid", "", "vm uuid")
}
