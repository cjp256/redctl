// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string

	// rootCmd represents the base command when called without any subcommands
	rootCmd = &cobra.Command{
		Use:   "redctl",
		Short: "Command line interface for all redfield redctl",
		Long:  ``,
		// Uncomment the following line if your bare application
		// has an action associated with it:
		//	Run: func(cmd *cobra.Command, args []string) { },
	}
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "/storage/services/redctl/config.yaml", "Path to configuration file")
	rootCmd.PersistentFlags().Bool("verbose", true, "verbose")
	rootCmd.PersistentFlags().Bool("local", false, "Operate in local (unmanaged or detached mode)")
	rootCmd.PersistentFlags().String("server-url", "unix:///var/lib/redctld/redctl.socket", "Server Address (unix:///path or ip:port)")
	rootCmd.PersistentFlags().String("username", "", "username")
	rootCmd.PersistentFlags().String("password", "", "password")
	rootCmd.PersistentFlags().String("timeout", "300s", "timeout")
	rootCmd.PersistentFlags().String("device-uuid", "", "Device UUID for managed operations")
	rootCmd.PersistentFlags().String("tls-ca-certificate", "/storage/services/redctl/ca-certificate.pem", "CA certificate file for TLS")
	rootCmd.PersistentFlags().String("tls-certificate", "/storage/services/redctl/certificate.pem", "Client certificate file for TLS")
	rootCmd.PersistentFlags().String("tls-key", "/storage/services/redctl/device-key.pem", "Client key file for TLS")
	rootCmd.PersistentFlags().String("tls-server-host-override", "", "Server name to used to verify the hostname returned by TLS handshake")
	rootCmd.PersistentFlags().String("configuration-path-domains", "/storage/services/redctl/vms", "Path to store VM configurations")
	rootCmd.PersistentFlags().String("configuration-path-device", "/storage/services/redctl", "Path store device configuration")

	// bind all pflags to configuration file
	viper.BindPFlags(rootCmd.PersistentFlags())

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.SetConfigFile(cfgFile)
	viper.SetEnvPrefix("REDCTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv() // read in environment variables that match

	if err := viper.ReadInConfig(); err == nil {
		log.Println("Using config file:", viper.ConfigFileUsed())
	} else {
		log.Println("Failed to read configuration file:", err.Error())
	}
}
