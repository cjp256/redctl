module gitlab.com/redfield/redctl

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8 // indirect
	github.com/gofrs/flock v0.7.0
	github.com/golang/protobuf v1.2.0
	github.com/google/uuid v1.1.0
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	golang.org/x/net v0.0.0-20190206173232-65e2d4e15006
	google.golang.org/grpc v1.18.0
)
