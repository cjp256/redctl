GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/redctl cmd/redctl/*.go
	go build -o bin/redctld cmd/redctld/*.go

.PHONY: install
install:
	install -d -m 0755 $(DESTDIR)/etc/bash_completion.d
	install -m 0755 configs/redctl.bash_completion.sh $(DESTDIR)/etc/bash_completion.d/

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ internal/ test/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/redctl.pb.go: api/redctl.proto
	protoc -I api --go_out=plugins=grpc:api api/redctl.proto

.PHONY: proto
proto: api/redctl.pb.go

.PHONY: check
check: goreportcard all test
	DESTDIR=/tmp make install

.PHONY: test
test:
	#go test -v internal/device/*
	#go test -v internal/domain/*
	#go test -v internal/xl/*
	go test -v internal/lock/*

.PHONY: goreportcard
goreportcard: gofmt govet gocyclo golint ineffassign misspell

.PHONY: gofmt
gofmt:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gofmt

.PHONY: govet
govet:
	go tool vet api/ cmd/ internal/

.PHONY: gocyclo
gocyclo:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gocyclo --cyclo-over 15

.PHONY: golint
golint:
	gometalinter --deadline=90s --disable-all --vendor ./... -E golint --min-confidence=0.85

.PHONY: ineffassign
ineffassign:
	gometalinter --deadline=90s --disable-all --vendor ./... -E ineffassign

.PHONY: misspell
misspell:
	gometalinter --deadline=90s --disable-all --vendor ./... -E misspell
